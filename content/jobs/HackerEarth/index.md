---
date: '2019-01-10'
title: 'Campus Ambassador'
company: 'HackerEarth'
location: 'Ranchi'
range: 'January - August 2019'
url: 'https://www.hackerearth.com/'
---

- Held online hackathon on Machine Learning. Create a HackerEarth Facebook page/group for my college and use it to promote activities and information about HackerEarth in my college.