---
date: '2019-06-26'
title: 'Intel Software Innovator'
company: 'Intel'
location: '#'
range: 'June - Present'
url: 'https://software.intel.com/en-us/intel-software-innovators'
---

- The Intel® Software Innovator program supports innovative, independent developers who display an ability to create and demonstrate forward-looking projects by providing them with speaking and demonstration opportunities at industry events and developer gatherings. Through their expertise and innovation with cutting-edge technology, Intel Software Innovators demonstrate a spirit of ingenuity, experimentation, and forward thinking that inspires the greater developer community.