---
date: '2019-06-01'
title: 'Marketting Intern'
company: 'Jindal Intellicom'
location: 'Gurgaon'
range: 'June - July 2019'
url: 'http://careers.intellicomcenters.com/'
---

- Collected quantitative and qualitative data from marketing campaigns. Performed market analysis and research on competition. Supported the marketing team in daily administrative tasks