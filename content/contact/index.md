---
title: 'Get In Touch'
---

I am looking for internship/Research internship opportunities, my inbox is always open. Whether for a potential project or just to say hi, I'll try my best to answer your email!
