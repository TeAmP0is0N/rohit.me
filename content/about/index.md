---
title: 'About Me'
avatar: './me.jpg'
skills:
  - JavaScript (Starter)
  - HTML & (S)CSS
  - React (Starter)
  - C/CPP
  - Node.js (Starter)
  - Python
  - jQuery
  - BootStrap
  - Git
  - Heroku
---

Hello, I am Rohit Rathore. I am an Integrated MSc student in Mathematics and Computing. I have completed my 1st year. I am a computer science enthusiast and passionate programmer, involved in Cyber Security and Artificial Intelligence.

I am passionate about programming and delivering, clean, test-covered, well-designed and scalable code. I seek to learn and try to apply all of it in a way that makes life easier. I always try to design, develop and deliver technology solutions to problems around me. I develop websites and web apps that provide intuitive, pixel-perfect user interfaces with efficient and modern backends.

After completing my schooling from RavindraNath Tagore Education Center Inter College, I joined [Birla Institute of Technology, Mesra](https://www.bitmesra.ac.in/) for Integrated MSc in Mathematics and Computer Science. It's a five year Integrted course. 

Here's a few technologies I've been working with recently:
