---
date: '1'
title: 'BookToRead'
cover: './booktoread.png'
github: 'https://github.com/TeAmP0is0N/book-to-read'
external: 'https://book-to-read.herokuapp.com'
tech:
  - React.js
showInProjects: true
---

Book To Read app made with React to add the title of those books which you wanna read.