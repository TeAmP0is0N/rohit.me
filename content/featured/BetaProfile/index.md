---
date: '3'
title: 'BetaProfile'
cover: './betaprofile.png'
github: 'https://github.com/TeAmp0is0N/BetaProfile'
external: 'https://betaprofile.glob4lh3ll.now.sh/'
tech:
  - Next.js
  - Chart.js
  - GitHub API
showInProjects: true
---

A nicer look at your GitHub profile and repo stats. Includes data visualizations of your top languages, starred repositories, and sort through your top repos by number of stars, forks, and size.
