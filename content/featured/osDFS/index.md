---
date: '2'
title: 'osDFS'
cover: './osdfs.png'
github: 'https://github.com/TeAmP0is0N/GirlsInTech'
external: 'http://osdfs.in/'
tech:
  - Jekyll
  - BootStrap
showInProjects: true
---

Coded osDFS webpage which is a special interest group that supports women & men who study or are interested in Computer and Information Science.