---
date: '2019-08-10'
title: 'Personal Website'
github: 'https://github.com/TeAmP0is0N/rohit-rathaur.github.io'
external: 'https://teamp0is0n.github.io/rohit-rathaur.github.io/'
tech:
  - HTML
  - CSS
  - JS
  - Bootstrap
showInProjects: true
---

My first portfolio website I designed and built in 2019. I learned quite a bit about HTML, CSS, and JS. Since then, I think my web development and design skills have improved immensely.
