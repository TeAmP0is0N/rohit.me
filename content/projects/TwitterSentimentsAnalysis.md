---
date: '2019-05-10'
title: 'Falling Words'
github: ''
external: 'https://www.researchgate.net/publication/334453393_twitter_sentiments_analysis'
tech:
  - Machine Learning
  - Python
showInProjects: true
---

Twitter is a popular microblogging service where users create status messages (called tweets). These tweets sometimes express opinions about different topics. The purpose of this project is to build an algorithm that can accurately classify Twitter messages as positive or negative, with respect to a query term. Our hypothesis is that we can obtain high accuracy on classifying sentiment in Twitter messages using machine learning techniques. Generally, this type of sentiment analysis is useful for consumers who are trying to research a product or service, or marketers researching public opinion of their company.