---
date: '2019-08-10'
title: 'Falling Words'
github: 'https://github.com/TeAmP0is0N/RishikaNew'
external: 'https://teamp0is0n.github.io/RishikaNew/'
tech:
  - HTML
  - CSS Transitions
  - JS
showInProjects: true
---

Gold winning entry to the Hacktoberfest, Coded a basic interactive game using HTML, CSS transitions and JavaScript and I chose not to use the canvas element for it. I was curious to see how far I could get using basic web technologies. I knew performance would be probably very the critical part, but that made it even more challenging! During this journe I’ve learned a few interesting things on the way