---
title: 'osDFS'
cover: './osdfs.png'
github: ''
external: 'http://osdfs.in/'
tech:
  - Jekyll
  - BootStrap
showInProjects: true
---

Coded osDFS webpage which is a special interest group that supports women & men who study or are interested in Computer and Information Science.