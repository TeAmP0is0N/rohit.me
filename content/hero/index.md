---
title: 'Hi, my name is'
name: 'Rohit Singh Rathaur'
subtitle: 'I build things for the web.'
contactText: 'Get In Touch'
---

I'm an Integrated MSc student in Mathematics and Computing at Birla Institute of Technology, Mesra. I am learning to build (and occasionally designing) exceptional, high-quality websites and applications.
