module.exports = {
  siteTitle: 'Rohit Rathore | Cyber Security',
  siteDescription:
    'Hello, I am Rohit Rathore. I am an Integrated MSc student in Mathematics and Computing. I have completed my 1st year. I am a computer science enthusiast and passionate programmer, involved in Cyber Security and Artificial Intelligence.',
  siteKeywords:
    'Rohit Rathore, Rohit, Rathore, cyber security, Artificial Intelligence, web developer, javascript, northIndian',
  siteUrl: 'https://github.com/TeAmP0is0N/rohit.me',
  siteLanguage: 'en_US',
  name: 'Rohit Rathore',
  location: 'Ranchi, JH',
  email: 'rohitrathore.imh55@gmail.com',
  github: 'https://github.com/TeAmP0is0N',
  twitterHandle: '@rohit_rathore2',
  socialMedia: [
    {
      name: 'Github',
      url: 'https://github.com/TeAmP0is0N',
    },
    {
      name: 'Linkedin',
      url: 'https://www.linkedin.com/in/rohit-singh-rathaur/',
    },
    {
      name: 'Codepen',
      url: 'https://codepen.io/glob4lh3ll',
    },
    {
      name: 'Instagram',
      url: 'https://www.instagram.com/rohit_rathore2/',
    },
    {
      name: 'Twitter',
      url: 'https://twitter.com/rohit_rathore2',
    },
  ],

  navLinks: [
    {
      name: 'About',
      url: '#about',
    },
    {
      name: 'Experience',
      url: '#jobs',
    },
    {
      name: 'Work',
      url: '#projects',
    },
    {
      name: 'Contact',
      url: '#contact',
    },
  ],

  navHeight: 100,
  greenColor: '#64ffda',
  navyColor: '#0a192f',
  darkNavyColor: '#020c1b',

  srConfig: (delay = 200) => ({
    origin: 'bottom',
    distance: '20px',
    duration: 500,
    delay,
    rotate: { x: 0, y: 0, z: 0 },
    opacity: 0,
    scale: 1,
    easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
    mobile: true,
    reset: false,
    useDelay: 'always',
    viewFactor: 0.25,
    viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },
  }),
};
